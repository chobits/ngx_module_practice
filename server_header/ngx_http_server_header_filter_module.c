
/*
 * for nginx-1.2.7, change "Server: ..." header of http response
 */


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>


static ngx_int_t ngx_http_server_header_filter_init(ngx_conf_t *cf);
static void * ngx_http_server_header_filter_create_loc_conf(ngx_conf_t *cf);
static char * ngx_http_server_header_filter_merge_loc_conf(ngx_conf_t *cf,
    void *parent, void *child);


static ngx_http_output_header_filter_pt	ngx_http_next_header_filter;


typedef struct {
    ngx_str_t       server_str;
} ngx_http_server_header_filter_loc_conf_t;


static ngx_http_module_t ngx_http_server_header_filter_module_ctx = {
    NULL,                                           /* proconfiguration */
    ngx_http_server_header_filter_init,             /* postconfiguration */

    NULL,                                           /* create main configuration */
    NULL,                                           /* init main configuration */

    NULL,                                           /* create server configuration */
    NULL,                                           /* merge server configuration */

    ngx_http_server_header_filter_create_loc_conf,  /* create location configuration */
    ngx_http_server_header_filter_merge_loc_conf,   /* merge location configuration */
};


ngx_command_t ngx_http_server_header_filter_commands[] = {

    { ngx_string("server_header"),
      NGX_HTTP_MAIN_CONF|NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_str_slot,
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_server_header_filter_loc_conf_t, server_str),
      NULL },

      ngx_null_command
};


ngx_module_t ngx_http_server_header_filter_module = {
    NGX_MODULE_V1,
    &ngx_http_server_header_filter_module_ctx,  /* module context */
    ngx_http_server_header_filter_commands,     /* module directives */
    NGX_HTTP_MODULE,                            /* module type */
    NULL,                                       /* init master */
    NULL,                                       /* init module */
    NULL,                                       /* init process */
    NULL,                                       /* init thread */
    NULL,                                       /* exit thread */
    NULL,                                       /* exit process */
    NULL,                                       /* exit master */
    NGX_MODULE_V1_PADDING
};


static void *
ngx_http_server_header_filter_create_loc_conf(ngx_conf_t *cf)
{
    return ngx_pcalloc(cf->pool, sizeof(ngx_http_server_header_filter_loc_conf_t));
}


static char *
ngx_http_server_header_filter_merge_loc_conf(ngx_conf_t *cf, void *parent, void *child)
{
    ngx_http_server_header_filter_loc_conf_t    *prev = parent;
    ngx_http_server_header_filter_loc_conf_t    *conf = child;

    ngx_conf_merge_str_value(conf->server_str, prev->server_str, "");

    return NGX_CONF_OK;
}


static ngx_int_t
ngx_http_server_header_filter(ngx_http_request_t *r)
{
    ngx_http_server_header_filter_loc_conf_t   *sflcf;

    /* It doesnt change server header of subrequest. */

    if (r != r->main) {
        return ngx_http_next_header_filter(r);
    }

    sflcf = ngx_http_get_module_loc_conf(r, ngx_http_server_header_filter_module);
    if (sflcf == NULL || sflcf->server_str.len == 0) {
        return ngx_http_next_header_filter(r);
    }

    r->headers_out.server = ngx_list_push(&r->headers_out.headers);
    if (r->headers_out.server == NULL) {
        return NGX_ERROR;
    }

    r->headers_out.server->hash = 1;
    ngx_str_set(&r->headers_out.server->key, "Server");     /* header key */
    r->headers_out.server->value = sflcf->server_str;       /* header value */
    
    return ngx_http_next_header_filter(r);
}


static ngx_int_t
ngx_http_server_header_filter_init(ngx_conf_t *cf)
{
    ngx_http_next_header_filter = ngx_http_top_header_filter;
    ngx_http_top_header_filter = ngx_http_server_header_filter;

    return NGX_OK;
}
