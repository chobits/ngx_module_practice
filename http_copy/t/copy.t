#!/usr/bin/perl

# (C) xiaochen

###############################################################################

use warnings;
use strict;

use Test::More;
use File::Copy;

BEGIN { use FindBin; chdir($FindBin::Bin); }

use lib 'lib';
use Test::Nginx;

###############################################################################

select STDERR; $| = 1;
select STDOUT; $| = 1;

# 3 means 3 like(..)
my $t = Test::Nginx->new()->plan(3)
	->write_file_expand('nginx.conf', <<'EOF');

master_process off;
daemon         off;

events {
}

http {
    access_log    off;

    # copy/proxy_pass sever
    server {
        listen       127.0.0.1:8080;
        server_name  localhost;
        root %%TESTDIR%%;

        location / {
		http_copy 127.0.0.1:8082 multiple=1;
		proxy_pass http://127.0.0.1:8081;
        }
    }

    # proxy_pass backend server
    server {
        listen       127.0.0.1:8081;
        server_name  localhost;
        root %%TESTDIR%%;

	location / {
		echo $arg_info;
	}
    }

    # http_copy backend server
    server {
        listen       127.0.0.1:8082;
        server_name  localhost;
        root %%TESTDIR%%;

	location / {
		return 200;
	}
    }
}

EOF


# test case 1

$t->run();

# test whether backend server can handle original request
like("ok", qr/ok/, 'ok');
like(http_get('/?info=test_backend_server'), qr/test_backend_server/, 'request backend server');
#my $rr = 
#print "$rr";
print http_get('/?info=backend_server_can_reponse_this_request');
like(http_get('/?info=backend_server_can_reponse_this_request'), qr/backend_server_can_reponse_this_request/, 'request backend server');


# test whether copied request is sent to backend server

$t->stop();
