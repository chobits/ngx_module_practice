
/*
 * in nginx-src, run: ./configure --add-module=../hello_handler_module
 *
 * nginx.conf
 * ....
 *       location /hello {
 *           hello_string "hello world!";
 *       }
 * ....
 */


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>


typedef struct {
    ngx_str_t hello_string;
} ngx_http_hello_loc_conf_t;


static ngx_int_t ngx_http_hello_init(ngx_conf_t *conf);
static void *ngx_http_hello_create_loc_conf(ngx_conf_t *conf);


/* define directive in nginx.conf */
static ngx_command_t ngx_http_hello_commands[] = {

    { ngx_string("hello_string"),
      NGX_HTTP_LOC_CONF|NGX_CONF_NOARGS|NGX_CONF_TAKE1,
      ngx_conf_set_str_slot, /* ngx_http_hello_string */
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_hello_loc_conf_t, hello_string),
      NULL },

    ngx_null_command
};


static ngx_http_module_t ngx_http_hello_module_ctx = {
    NULL,
    ngx_http_hello_init,
    NULL,
    NULL,
    NULL,
    NULL,
    ngx_http_hello_create_loc_conf,     /* used by main()->ngx_init_cycle():line 223 */
    NULL
};


ngx_module_t ngx_http_hello_module = {
    NGX_MODULE_V1,
    &ngx_http_hello_module_ctx,         /* .ctx: module's custom context */
    ngx_http_hello_commands,            /* .cmd  */
    NGX_HTTP_MODULE,                    /* .type */
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NGX_MODULE_V1_PADDING
};


/*
 * from bt command of gdb:
 *    ngx_http_handler
 *      -> ngx_http_core_run_phases
 *        -> ngx_http_core_content_phase
 *           -> ngx_http_hello_handler
 */
static
ngx_int_t ngx_http_hello_handler(ngx_http_request_t *req)
{
    ngx_buf_t                  *buf;
    ngx_http_hello_loc_conf_t  *hc;
    ngx_chain_t                 chain;
    ngx_int_t                   r, content_length = 0;
    u_char                     *ngx_hello_string;


    hc = ngx_http_get_module_loc_conf(req, ngx_http_hello_module);  /* module's custom conf */
    if (hc->hello_string.len == 0) {
        return NGX_DECLINED;
    }

    if (!(req->method & (NGX_HTTP_GET | NGX_HTTP_HEAD))) {
        return NGX_HTTP_NOT_ALLOWED;
    }

    r = ngx_http_discard_request_body(req);
    if (r != NGX_OK) {
        return r;
    }

    ngx_hello_string = hc->hello_string.data;
    content_length = hc->hello_string.len;

    if (req->method == NGX_HTTP_HEAD) {
        req->headers_out.status = NGX_HTTP_OK;
        req->headers_out.content_length_n = content_length;
        return ngx_http_send_header(req);
    }

    /* send header */
    req->headers_out.status = NGX_HTTP_OK;
    req->headers_out.content_length_n = content_length;     /* tell nginx that content length has been changed */
    ngx_str_set(&req->headers_out.content_type, "text/html");

    r = ngx_http_send_header(req);
    if (r == NGX_ERROR || r > NGX_OK || req->header_only) {
        return r;
    }

    /* send body */
    buf = ngx_pcalloc(req->pool, sizeof(ngx_buf_t));

    if (!buf) {
        return NGX_HTTP_INTERNAL_SERVER_ERROR;
    }

    chain.buf = buf;
    chain.next = NULL;

    buf->pos = ngx_hello_string;
    buf->last = ngx_hello_string + content_length;
    buf->memory = 1;    /* read-only memory[pos - last] */
    buf->last_buf = 1;  /* no more buffers in the request */

    /* call filter directly */
    return ngx_http_output_filter(req, &chain);
}


/* create module's custom arguments struct (here is hello module) */
static
void *ngx_http_hello_create_loc_conf(ngx_conf_t *cf)
{
    /* pcalloc will memzero conf */
    return ngx_pcalloc(cf->pool, sizeof(ngx_http_hello_loc_conf_t));
}


static
ngx_int_t ngx_http_hello_init(ngx_conf_t *conf)
{
    ngx_http_handler_pt        *h;
    ngx_http_core_main_conf_t  *cmcf;

    /* add hello handler into http content phase handlers */
    cmcf = ngx_http_conf_get_module_main_conf(conf, ngx_http_core_module);

    h = ngx_array_push(&cmcf->phases[NGX_HTTP_CONTENT_PHASE].handlers);
    if (!h) {
        return NGX_ERROR;
    }

    *h = ngx_http_hello_handler;

    return NGX_OK;
}
