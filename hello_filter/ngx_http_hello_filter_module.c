
/*
 * for nginx-1.2.7
 * in nginx-src, run: ./configure --add-module=../hello_filter_module
 */


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>


static ngx_int_t ngx_http_hello_filter_init(ngx_conf_t *cf);
static void * ngx_http_hello_filter_create_loc_conf(ngx_conf_t *cf);
static char * ngx_http_hello_filter_merge_loc_conf(ngx_conf_t *cf, void *parent,
    void *child);


static ngx_http_output_header_filter_pt	ngx_http_next_header_filter;
static ngx_http_output_body_filter_pt	ngx_http_next_body_filter;


typedef struct {
    ngx_str_t       append_str;
    ngx_hash_t      types;
    ngx_array_t    *types_keys;
} ngx_http_hello_filter_loc_conf_t;


typedef struct {
    ngx_str_t       append_str;
} ngx_http_hello_filter_ctx_t;


static ngx_http_module_t ngx_http_hello_filter_module_ctx = {
    NULL,                                   /* proconfiguration */
    ngx_http_hello_filter_init,             /* postconfiguration */

    NULL,                                   /* create main configuration */
    NULL,                                   /* init main configuration */

    NULL,                                   /* create server configuration */
    NULL,                                   /* merge server configuration */

    ngx_http_hello_filter_create_loc_conf,  /* create location configuration */
    ngx_http_hello_filter_merge_loc_conf    /* merge location configuration */
};


ngx_command_t ngx_http_hello_filter_commands[] = {

    { ngx_string("append"),
      NGX_HTTP_MAIN_CONF|NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_str_slot,
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_hello_filter_loc_conf_t, append_str),
      NULL },

    { ngx_string("append_types"),
      NGX_HTTP_MAIN_CONF|NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_1MORE,
      ngx_http_types_slot,
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_hello_filter_loc_conf_t, types_keys),
      &ngx_http_html_default_types[0] },

      ngx_null_command
};


ngx_module_t ngx_http_hello_filter_module = {
    NGX_MODULE_V1,
    &ngx_http_hello_filter_module_ctx,  /* module context */
    ngx_http_hello_filter_commands,     /* module directives */
    NGX_HTTP_MODULE,                    /* module type */
    NULL,                               /* init master */
    NULL,                               /* init module */
    NULL,                               /* init process */
    NULL,                               /* init thread */
    NULL,                               /* exit thread */
    NULL,                               /* exit process */
    NULL,                               /* exit master */
    NGX_MODULE_V1_PADDING
};


static void *
ngx_http_hello_filter_create_loc_conf(ngx_conf_t *cf)
{
    return ngx_pcalloc(cf->pool, sizeof(ngx_http_hello_filter_loc_conf_t));
}


static char *
ngx_http_hello_filter_merge_loc_conf(ngx_conf_t *cf, void *parent, void *child)
{
    ngx_http_hello_filter_loc_conf_t    *prev = parent;
    ngx_http_hello_filter_loc_conf_t    *conf = child;

    ngx_conf_merge_str_value(conf->append_str, prev->append_str, "");

    if (ngx_http_merge_types(cf, &conf->types_keys, &conf->types,
                             &prev->types_keys, &prev->types,
                             ngx_http_html_default_types)
        != NGX_OK)
    {
        return NGX_CONF_ERROR;
    }

    return NGX_CONF_OK;
}


static ngx_int_t
ngx_http_hello_header_filter(ngx_http_request_t *r)
{
    ngx_http_hello_filter_loc_conf_t   *hflcf;
    ngx_http_hello_filter_ctx_t        *ctx;

    if (r->headers_out.status != NGX_HTTP_OK || r != r->main) {
        return ngx_http_next_header_filter(r);
    }

    hflcf = ngx_http_get_module_loc_conf(r, ngx_http_hello_filter_module);
    if (hflcf == NULL || hflcf->append_str.len == 0) {
        return ngx_http_next_header_filter(r);
    }

    if (ngx_http_test_content_type(r, &hflcf->types) == NULL) {
        return ngx_http_next_header_filter(r);
    }

    ctx = ngx_pcalloc(r->pool, sizeof(ngx_http_hello_filter_ctx_t));
    if (ctx == NULL) {
        return NGX_ERROR;
    }

    ctx->append_str.data = hflcf->append_str.data;
    ctx->append_str.len = hflcf->append_str.len;

    ngx_http_set_ctx(r, ctx, ngx_http_hello_filter_module);

    /*
     * You must use either #1 or #2 in order to
     * tell nginx that http content length has been changed.
     */

    /* #1 */
    if (r->headers_out.content_length_n != -1) {
        r->headers_out.content_length_n += hflcf->append_str.len;
    }

    /* #2
     * If we clear content length, chunked filter will run.
     * ngx_http_clear_content_length(r);
     */

    return ngx_http_next_header_filter(r);
}


/* padding string to http content */
static ngx_int_t
ngx_http_hello_body_filter(ngx_http_request_t *r, ngx_chain_t *in)
{
    ngx_buf_t                          *b;
    ngx_chain_t                        *chain, *add_chain;
    ngx_http_hello_filter_ctx_t        *ctx;
    ngx_uint_t                          last_buffer;

    ctx = ngx_http_get_module_ctx(r, ngx_http_hello_filter_module);
    if (ctx == NULL) {
        return ngx_http_next_body_filter(r, in);
    }

    /* find last buffer */
    last_buffer = 0;

    for (chain = in; chain; chain = chain->next) {
        if (chain->buf->last_buf) {
            last_buffer = 1;
            break;
        }
    }

    if (!last_buffer) {
        return ngx_http_next_body_filter(r, in);
    }


    /* append content to http response */
    b = ngx_calloc_buf(r->pool);
    if (!b) {
        return NGX_ERROR;
    }

    b->pos = ctx->append_str.data;
    b->last = b->pos + ctx->append_str.len;
    b->start = b->pos;
    b->end = b->last;
    b->last_buf = 1;
    b->memory = 1;

    if (ngx_buf_size(chain->buf) == 0) {
        chain->buf = b;
    } else {
        add_chain = ngx_alloc_chain_link(r->pool);
        if (!add_chain) {
            return NGX_ERROR;
        }

        add_chain->buf = b;
        add_chain->next = NULL;

        chain->next = add_chain;
        chain->buf->last_buf = 0;
    }

    return ngx_http_next_body_filter(r, in);
}


static ngx_int_t
ngx_http_hello_filter_init(ngx_conf_t *cf)
{
    ngx_http_next_header_filter = ngx_http_top_header_filter;
    ngx_http_top_header_filter = ngx_http_hello_header_filter;

    ngx_http_next_body_filter = ngx_http_top_body_filter;
    ngx_http_top_body_filter = ngx_http_hello_body_filter;

    return NGX_OK;
}
