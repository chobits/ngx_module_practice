
/*
 * for nginx-1.2.7
 * use nginx subrequest mechanism to implement footer filter
 * in nginx-src, run: ./configure --add-module=../hello_filter_module
 */


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>


static ngx_uint_t in_subrequest = 0;
static ngx_int_t ngx_http_hello_filter_init(ngx_conf_t *cf);


static ngx_http_module_t ngx_http_hello_filter_module_ctx = {
    NULL,
    ngx_http_hello_filter_init,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};


ngx_module_t ngx_http_hello_filter_module = {
    NGX_MODULE_V1,
    &ngx_http_hello_filter_module_ctx,  /* .ctx: module's custom context */
    NULL,                               /* .cmd */
    NGX_HTTP_MODULE,                    /* .type */
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NGX_MODULE_V1_PADDING
};


static ngx_http_output_header_filter_ptngx_http_next_header_filter;
static ngx_http_output_body_filter_ptngx_http_next_body_filter;


static
ngx_int_t ngx_http_hello_header_filter(ngx_http_request_t *r)
{
    if (r->headers_out.status != NGX_HTTP_OK || r != r->main) {
        return ngx_http_next_header_filter(r);
    }

    in_subrequest = 1;
    ngx_http_clear_content_length(r);
    ngx_http_clear_accept_ranges(r);

    return ngx_http_next_header_filter(r);
}


static
ngx_int_t ngx_http_hello_body_filter(ngx_http_request_t *r, ngx_chain_t *in)
{
    ngx_int_t               rc;
    ngx_uint_t              last;
    ngx_str_t               uri;
    ngx_http_request_t     *sr;    /* return value of ngx_http_subrequest() */
    ngx_chain_t            *cl;

    if (in == NULL || r->header_only || !in_subrequest) {
        return ngx_http_next_body_filter(r, in);
    }

    /* set current request not end */
    last = 0;
    for (cl = in; cl; cl = cl->next) {
        if (cl->buf->last_buf) {
            cl->buf->last_buf = 0;
            cl->buf->sync = 1;
            last = 1;
            break;
        }
    }

    /* send current request */
    rc = ngx_http_next_body_filter(r, in);
    if (!last || rc == NGX_ERROR) {
        return rc;
    }

    /* append sub request */
    ngx_str_set(&uri, "/hello_filter.html");
    if (ngx_http_subrequest(r, &uri, NULL, &sr, NULL, 0) != NGX_OK) {
        return NGX_ERROR;
    }

    /*
     * clear it, when subrequest iter into this function 
     * TODO: use r->ctx[..] insteal of global @in_subrequest
     */
    in_subrequest = 0;

    /* send sub request */
    return ngx_http_send_special(r, NGX_HTTP_LAST);
}


static
ngx_int_t ngx_http_hello_filter_init(ngx_conf_t *cf)
{
    /* int ngx_http_next_*_filter */
    ngx_http_next_header_filter = ngx_http_top_header_filter;
    ngx_http_top_header_filter = ngx_http_hello_header_filter;

    ngx_http_next_body_filter = ngx_http_top_body_filter;
    ngx_http_top_body_filter = ngx_http_hello_body_filter;

    return NGX_OK;
}
