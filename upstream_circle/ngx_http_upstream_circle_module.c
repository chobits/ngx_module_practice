
/*
 * Hash a variable to choose an upstream server.
 */


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>


typedef struct {
    struct sockaddr    *sockaddr;
    socklen_t           socklen;
    ngx_str_t           name;
} ngx_http_upstream_circle_peer_t;


typedef struct {
    ngx_uint_t                          number;
    ngx_uint_t                          current;
    ngx_http_upstream_circle_peer_t     peer[1];
} ngx_http_upstream_circle_peers_t;     /* one per worker process */


typedef struct {
    ngx_http_upstream_circle_peers_t   *peers;
    uintptr_t                           tried[1];
} ngx_http_upstream_circle_peer_data_t; /* one per http request */


static char *ngx_http_upstream_circle(ngx_conf_t *cf, ngx_command_t *cmd,
    void *conf);
static ngx_int_t ngx_http_upstream_init_circle_peer(ngx_http_request_t *r,
    ngx_http_upstream_srv_conf_t *uscf);
static void ngx_http_upstream_free_circle_peer(ngx_peer_connection_t *pc,
    void *data, ngx_uint_t state);
static ngx_int_t ngx_http_upstream_get_circle_peer(ngx_peer_connection_t *pc,
    void *data);


static ngx_command_t  ngx_http_upstream_circle_commands[] = {
    { ngx_string("circle"),
      NGX_HTTP_UPS_CONF|NGX_CONF_NOARGS,
      ngx_http_upstream_circle,
      0,
      0,
      NULL },

      ngx_null_command
};


static ngx_http_module_t  ngx_http_upstream_circle_module_ctx = {
    NULL,                               /* preconfiguration */
    NULL,                               /* postconfiguration */

    NULL,                               /* create main configuration */
    NULL,                               /* init main configuration */

    NULL,                               /* create server configuration */
    NULL,                               /* merge server configuration */

    NULL,                               /* create location configuration */
    NULL                                /* merge location configuration */
};


ngx_module_t  ngx_http_upstream_circle_module = {
    NGX_MODULE_V1,
    &ngx_http_upstream_circle_module_ctx,   /* module context */
    ngx_http_upstream_circle_commands,      /* module directives */
    NGX_HTTP_MODULE,                        /* module type */
    NULL,                                   /* init master */
    NULL,                                   /* init module */
    NULL,                                   /* init process */
    NULL,                                   /* init thread */
    NULL,                                   /* exit thread */
    NULL,                                   /* exit process */
    NULL,                                   /* exit master */
    NGX_MODULE_V1_PADDING
};


static ngx_int_t
ngx_http_upstream_init_circle(ngx_conf_t *cf, ngx_http_upstream_srv_conf_t *uscf)
{
    ngx_uint_t                              i, j, n;
    ngx_http_upstream_server_t             *server;
    ngx_http_upstream_circle_peers_t       *peers;

    /* calculate the number of upstream servers */
    if (!uscf->servers) {
        return NGX_ERROR;
    }

    server = uscf->servers->elts;

    for (n = 0, i = 0; i < uscf->servers->nelts; i++) {
        n += server[i].naddrs;
    }

    /* init upstream peer data */
    peers = ngx_pcalloc(cf->pool, sizeof(ngx_http_upstream_circle_peers_t)
                        + sizeof(ngx_http_upstream_circle_peer_t) * (n - 1));

    if (peers == NULL) {
        return NGX_ERROR;
    }

    peers->number = n;

    /* one hostname can have multiple IP addresses in DNS */
    for (n = 0, i = 0; i < uscf->servers->nelts; i++) {
        for (j = 0; j < server[i].naddrs; j++, n++) {
            peers->peer[n].sockaddr = server[i].addrs[j].sockaddr;
            peers->peer[n].socklen = server[i].addrs[j].socklen;
            peers->peer[n].name = server[i].addrs[j].name;
        }
    }

    /* init upstream conf */
    uscf->peer.data = peers;    /* peers can be got from peer.init() */
    uscf->peer.init = ngx_http_upstream_init_circle_peer;

    return NGX_OK;
}


static ngx_int_t
ngx_http_upstream_init_circle_peer(ngx_http_request_t *r,
    ngx_http_upstream_srv_conf_t *uscf)
{
    ngx_http_upstream_circle_peer_data_t   *ucpd;
    ngx_http_upstream_circle_peers_t       *peers;

    ucpd = r->upstream->peer.data;
    peers = uscf->peer.data;

    if (ucpd == NULL) {
        /* alloc peer data and bitmap */
        ucpd = ngx_pcalloc(r->pool, sizeof(ngx_http_upstream_circle_peer_data_t)
                + peers->number / (sizeof(uintptr_t) * 8));

        if (ucpd == NULL) {
            return NGX_ERROR;
        }

        r->upstream->peer.data = ucpd;
    }

    ucpd->peers = uscf->peer.data;

    r->upstream->peer.free = ngx_http_upstream_free_circle_peer;
    r->upstream->peer.get = ngx_http_upstream_get_circle_peer;
    r->upstream->peer.tries = ucpd->peers->number;  /* try at most @number times */

    return NGX_OK;
}


static ngx_int_t
ngx_http_upstream_get_circle_peer(ngx_peer_connection_t *pc, void *data)
{
    ngx_http_upstream_circle_peer_data_t   *ucpd = data;
    ngx_http_upstream_circle_peers_t       *peers;
    ngx_http_upstream_circle_peer_t        *peer;
    ngx_uint_t                              i, m, n, current;

    ngx_log_debug1(NGX_LOG_DEBUG_HTTP, pc->log, 0,
                   "get upstream request circle peer try %ui", pc->tries);


    pc->cached = 0;
    pc->connection = NULL;

    /* find an usable peer */
    peers = ucpd->peers;

    current = peers->current;

    for (i = 0; i < peers->number ; i++) {

        peer = &peers->peer[current];

        n = current / (sizeof(uintptr_t) * 8);
        m = 1 << current % (sizeof(uintptr_t) * 8);

        current = (current + 1) % peers->number;

        if (!(ucpd->tried[n] & m)) {
            break;
        }

    }

    if (i == peers->number) {
        return NGX_BUSY;
    }

    /* get a peer */
    peers->current = current;

    ucpd->tried[n] |= m;

    pc->sockaddr = peer->sockaddr;
    pc->socklen = peer->socklen;
    pc->name = &peer->name;

    return NGX_OK;
}


static void
ngx_http_upstream_free_circle_peer(ngx_peer_connection_t *pc, void *data,
        ngx_uint_t state)
{
    ngx_http_upstream_circle_peer_data_t   *ucpd = data;

    /* TODO: add max_fails support */
    if (state == 0 && pc->tries == 0) {
        return;
    }

    if (ucpd->peers->number == 1) {
        pc->tries = 0;
        return;
    }

    if (pc->tries) {
        pc->tries--;
    }
}


static char *
ngx_http_upstream_circle(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ngx_http_upstream_srv_conf_t        *uscf;

    uscf = ngx_http_conf_get_module_srv_conf(cf, ngx_http_upstream_module);

    uscf->peer.init_upstream = ngx_http_upstream_init_circle;

    uscf->flags = NGX_HTTP_UPSTREAM_CREATE;

    return NGX_CONF_OK;
}
