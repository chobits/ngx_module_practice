/*
 * Copyright (C) xiaochen
 */

#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>


typedef struct {
    ngx_http_upstream_peer_t    peer;
    int                         ngx_http_upstream_cell_diamond_list;
    ngx_int_t                   cell_index;
    ngx_hash_t                 *cell_list;      /* from diamond server */
    ngx_array_t                *server_list;    /* intersection of cell_list and upstream server list */
    void                       *ctx;
    ngx_pool_t                 *pool;
} ngx_http_upstream_cell_conf_t;

typedef struct {
    ngx_str_t                       cell;
    ngx_http_upstream_srv_conf_t   *uscf;
} ngx_cell_servers_t;   /* element of server_list */

typedef struct {
    in_addr_t   ip;
    ngx_uint_t  mask;
} ngx_ip_mask_t;


static char * ngx_http_upstream_cell(ngx_conf_t *cf, ngx_command_t *cmd,
    void *conf);
static void * ngx_http_upstream_cell_create_conf(ngx_conf_t *cf);
static ngx_int_t ngx_http_upstream_init_cell_peer(ngx_http_request_t *r,
    ngx_http_upstream_srv_conf_t *us);

static ngx_command_t ngx_http_upstream_cell_commands[] = {

    { ngx_string("cell"),
      NGX_HTTP_UPS_CONF|NGX_CONF_NOARGS,
      ngx_http_upstream_cell,
      NGX_HTTP_SRV_CONF_OFFSET,
      0,
      NULL },

      ngx_null_command
};


static ngx_http_module_t  ngx_http_upstream_cell_module_ctx = {
    NULL,                                  /* preconfiguration */
    NULL,                                  /* postconfiguration */

    NULL,                                  /* create main configuration */
    NULL,                                  /* init main configuration */

    ngx_http_upstream_cell_create_conf,    /* create server configuration */
    NULL,                                  /* merge server configuration */

    NULL,                                  /* create location configuration */
    NULL                                   /* merge location configuration */
};


ngx_module_t  ngx_http_upstream_cell_module = {
    NGX_MODULE_V1,
    &ngx_http_upstream_cell_module_ctx,    /* module context */
    ngx_http_upstream_cell_commands,       /* module directives */
    NGX_HTTP_MODULE,                       /* module type */
    NULL,                                  /* init master */
    NULL,                                  /* init module */
    NULL,                                  /* init process */
    NULL,                                  /* init thread */
    NULL,                                  /* exit thread */
    NULL,                                  /* exit process */
    NULL,                                  /* exit master */
    NGX_MODULE_V1_PADDING
};


static void *
ngx_http_upstream_cell_create_conf(ngx_conf_t *cf)
{
    ngx_http_upstream_cell_conf_t *conf;

    conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_upstream_cell_conf_t));
    if (conf == NULL) {
        /* for error handing */
        return NULL;
    }

    return conf;
}


static ngx_int_t
ngx_http_upstream_init_cell(ngx_conf_t *cf, ngx_http_upstream_srv_conf_t *us)
{
    ngx_http_upstream_cell_conf_t  *ccf;
    ngx_str_t                       var;

    ccf = us->srv_conf[ngx_http_upstream_cell_module.ctx_index];

    /* hook us->peer.init */
    ccf->peer.init = us->peer.init;
    us->peer.init = ngx_http_upstream_init_cell_peer;

    ngx_str_set(&var, "cell");
    ccf->cell_index = ngx_http_get_variable_index(cf, &var);

    ccf->ctx = cf->ctx;

    /*  postpone running of ccf->peer.init_upstream() */

    return NGX_OK;
}


ngx_http_upstream_srv_conf_t *
ngx_http_upstream_servers_lookup(ngx_http_upstream_cell_conf_t *ccf,
    ngx_str_t *cell)
{
    ngx_uint_t                  i;
    ngx_cell_servers_t         *cell_servers;

    if (ccf->server_list == NULL) {
        return NULL;
    }

    cell_servers = ccf->server_list->elts;
    for (i = 0; i < ccf->server_list->nelts; i++) {
        if (cell->len == cell_servers[i].cell.len &&
            ngx_strncmp(cell->data, cell_servers[i].cell.data, cell->len) == 0) {
            return cell_servers[i].uscf;
        }
    }

    return NULL;
}


/* assert @cell not in @servers */
ngx_uint_t
ngx_http_upstream_servers_add(ngx_http_upstream_cell_conf_t *ccf,
    ngx_str_t *cell, ngx_http_upstream_srv_conf_t *uscf)
{
    ngx_cell_servers_t     *cell_servers;

    if (ccf->server_list == NULL) {
        ccf->server_list = ngx_array_create(ccf->pool, 16,
                                            sizeof(ngx_cell_servers_t));
        if (ccf->server_list == NULL) {
            return NGX_ERROR;
        }
    }

    cell_servers = ngx_array_push(ccf->server_list);
    if (cell_servers == NULL) {
        return NGX_ERROR;
    }

    cell_servers->cell.len = cell->len;
    cell_servers->cell.data = ngx_pstrdup(ccf->pool, cell);
    cell_servers->uscf = uscf;

    return NGX_OK;
}


ngx_array_t *
ngx_http_upstream_get_ip_list(ngx_http_upstream_cell_conf_t *ccf,
    ngx_http_request_t *r, ngx_str_t *cell)
{
    ngx_http_variable_value_t  *vv;
    ngx_str_t                   var;
    u_char                     *p, *last, *end, *bar, *cell_end;

    ngx_uint_t                  key;
    ngx_hash_key_t             *hk;
    ngx_hash_init_t             hash;

    ngx_array_t                 cell_list, *ip_list;
    ngx_ip_mask_t              *ip_mask;

    /* create cell list first time */
    if (ccf->cell_list == NULL) {

        ngx_str_set(&var, "router_rule");
        key = ngx_hash_key(var.data, var.len);    /* not necessary to use ngx_hash_strlow */
        vv = ngx_http_get_variable(r, &var, key);

        p = vv->data;
        last = vv->data + vv->len;

        if (ngx_array_init(&cell_list, ccf->pool, 32, sizeof(ngx_hash_key_t))
            != NGX_OK) {

            return NULL;
        }

        /* parse $router_rule */
        for (;;) {

            /* parse cell name */
            end = ngx_strnstr(p, ":", last - p);
            if (end == NULL) {
                return NULL;
            }

            /* parse end (e.g: "unitdb:true") */
            if (end[1] != '{'
                && (end - p) == (sizeof("unitdb") - 1)
                && ngx_strncmp(p, "unitdb", sizeof("unitdb") - 1) == 0) {

                break;
            }

            /* add this cell name */
            hk = ngx_array_push(&cell_list);
            if (hk == NULL) {
                return NULL;
            }

            hk->key.data = p;       /* ngx_hash_init() will alloc buffer for key */
            hk->key.len = end - p;
            hk->key_hash = ngx_hash_key_lc(p, end - p);

            p = end + 1;

            /* skip userid rule */
            end = ngx_strnstr(p, "}", last - p);
            p = end + 1;

            /* parse ip list */
            if (*p != '{') {
                return NULL;
            }
            p++;

            cell_end = ngx_strnstr(p, "};", last - p);
            if (cell_end == NULL) {
                return NULL;
            }

            ip_list = ngx_array_create(ccf->pool, 16, sizeof(ngx_ip_mask_t));

            for (;;) {
                end = ngx_strnstr(p, ",", cell_end - p) ? end : cell_end;

                ip_mask = ngx_array_push(ip_list);
                if (ip_mask == NULL) {
                    return NULL;
                }

                /* parse ip */
                bar = ngx_strnstr(p, "/", end - p);
                ip_mask->ip = ngx_inet_addr(p, bar - p);

                /* parse mask: ip is little endian(net order) */
                ip_mask->mask =
                    bar ? ((ngx_uint_t)(1 << ngx_atoi(bar + 1, end - bar - 1)) - 1)
                        : 0xffffffff;
                p = end + 1;

                if (end == cell_end) {
                    p = cell_end + 2;
                    break;
                }
            }

            hk->value = ip_list;
        }

        /* save ip list to hash bucket */
        ccf->cell_list = ngx_pcalloc(ccf->pool, sizeof(ngx_hash_t));
        if (ccf->cell_list == NULL) {
            return NULL;
        }

        hash.hash = ccf->cell_list;
        hash.key = ngx_hash_key_lc;
        hash.max_size = 512;
        hash.bucket_size = ngx_align(64, ngx_cacheline_size);
        hash.name = "upstream_cell_list_in_hash";
        hash.pool = ccf->pool;
        hash.temp_pool = NULL;

        if (ngx_hash_init(&hash, cell_list.elts, cell_list.nelts) != NGX_OK) {
            return NULL;
        }
    }

    key = ngx_hash_key_lc(cell->data, cell->len);
    return ngx_hash_find(ccf->cell_list, key, cell->data, cell->len);
}


ngx_uint_t
ngx_http_upstream_in_ip_list(ngx_http_upstream_server_t *server,
    ngx_array_t *ip_list)
{
    ngx_uint_t      server_ip, i;
    ngx_ip_mask_t  *ip_mask;

    server_ip = ((struct sockaddr_in *)server->addrs->sockaddr)->sin_addr.s_addr;
    ip_mask = ip_list->elts;

    for (i = 0; i < ip_list->nelts; i++) {
        if ((server_ip & ip_mask[i].mask) == (ip_mask[i].ip & ip_mask[i].mask)) {
            return 1;
        }
    }

    return 0;
}


ngx_http_upstream_srv_conf_t *
ngx_http_upstream_get_servers(ngx_http_upstream_cell_conf_t *ccf,
    ngx_http_upstream_srv_conf_t *us, ngx_http_request_t *r, ngx_str_t *cell)
{
    ngx_array_t                    *servers, *ori, *ip_list;
    ngx_http_upstream_server_t     *server, *ori_server;
    ngx_http_upstream_srv_conf_t   *uscf;
    ngx_conf_t                      cf;
    ngx_uint_t                      i;
    ngx_int_t                       rc;

    uscf = ngx_http_upstream_servers_lookup(ccf, cell);
    if (uscf != NULL) {
        return uscf;
    }


    /* autoconfig servers and save it */
    ori = us->servers;
    servers = ngx_array_create(ccf->pool, ori->nelts, sizeof(ngx_http_upstream_server_t));
    if (servers == NULL) {
        return NULL;
    }

    /* get ip list from $router_rule */
    ip_list = ngx_http_upstream_get_ip_list(ccf, r, cell);
    if (ip_list == NULL) {
        return NULL;
    }

    /* filter ip list to servers */
    ori_server = ori->elts;
    for (i = 0; i < ori->nelts; i++) {
        if (ngx_http_upstream_in_ip_list(&ori_server[i], ip_list)) {
            server = ngx_array_push(servers);
            if (server == NULL) {
                return NULL;
            }
            *server = ori_server[i];
        }
    }

    /* create new upstream server config */
    uscf = ngx_pcalloc(ccf->pool, sizeof(ngx_http_upstream_srv_conf_t));
    if (uscf == NULL) {
        return NULL;
    }

    *uscf = *us;
    uscf->servers = servers;

    cf.name = "upstream_cell";
    cf.pool = ccf->pool;
    cf.module_type = NGX_HTTP_MODULE;
    cf.cmd_type = NGX_HTTP_UPS_CONF;
    cf.log = ngx_cycle->log;
    cf.ctx = ccf->ctx;
    cf.args = NULL;

    /* reinit upstream server config */
    rc = ccf->peer.init_upstream(&cf, uscf);
    if (rc != NGX_OK) {
        return NULL;
    }


    if (ngx_http_upstream_servers_add(ccf, cell, uscf) != NGX_OK) {
        return NULL;
    }

    return uscf;
}

static ngx_int_t
ngx_http_upstream_get_cell(ngx_http_upstream_cell_conf_t *ccf,
    ngx_http_request_t *r, ngx_str_t *cell)
{
    ngx_http_variable_value_t  *vv;

    vv = ngx_http_get_indexed_variable(r, ccf->cell_index);
    if (vv == NULL) {
        return NGX_ERROR;
    }

    cell->data = vv->data;
    cell->len = vv->len;

    return NGX_OK;
}


static ngx_int_t
ngx_http_upstream_init_cell_peer(ngx_http_request_t *r,
    ngx_http_upstream_srv_conf_t *us)
{
    ngx_http_upstream_cell_conf_t  *ccf;
    ngx_http_upstream_srv_conf_t   *uscf;
    ngx_str_t                       cell;

    ccf = us->srv_conf[ngx_http_upstream_cell_module.ctx_index];

    if (ccf->pool == NULL) {
        ccf->pool = ngx_create_pool(4096, r->connection->log);
        if (ccf->pool == NULL) {
            return NGX_ERROR;
        }
    }

    if (ngx_http_upstream_get_cell(ccf, r, &cell) != NGX_OK) {
        return NGX_ERROR;
    }

    uscf = ngx_http_upstream_get_servers(ccf, us, r, &cell);
    if (uscf == NULL) {
        return NGX_HTTP_BAD_GATEWAY;
    }

    return uscf->peer.init(r, uscf);
}

static char *
ngx_http_upstream_cell(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ngx_http_upstream_srv_conf_t   *uscf;
    ngx_http_upstream_cell_conf_t  *ccf = conf;

    uscf = ngx_http_conf_get_module_srv_conf(cf, ngx_http_upstream_module);

    if (ccf->peer.init_upstream) {
        return "is duplicate";
    }

    /* hook uscf->peer.init_upstream */
    ccf->peer.init_upstream = uscf->peer.init_upstream;

    /* see ngx_http_upstream_init_main_conf() */
    if (ccf->peer.init_upstream == NULL) {
        ccf->peer.init_upstream = ngx_http_upstream_init_round_robin;
    }

    uscf->peer.init_upstream = ngx_http_upstream_init_cell; // only hooked once(when initting config)

    return NGX_CONF_OK;
}
